package dataLayer;

import Start.MainClass;
import businessLayer.Restaurant;

import java.awt.*;
import businessLayer.MenuItem;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class RestaurantSerializator {
    public static void readFile() throws IOException, ClassNotFoundException {
        FileInputStream file = new FileInputStream(MainClass.arg);
        ObjectInputStream objectInputStream = new ObjectInputStream(file);
        while(file.available() > 0){
            Restaurant.menu = (List<MenuItem>)objectInputStream.readObject();
        }
    }
}
