package dataLayer;

import Start.MainClass;
import businessLayer.MenuItem;



import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class FileWriter {

    public static void writeToFile(List<MenuItem> item) throws IOException {

        FileOutputStream file = new FileOutputStream(MainClass.arg);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
        objectOutputStream.writeObject(item);

    }

}
