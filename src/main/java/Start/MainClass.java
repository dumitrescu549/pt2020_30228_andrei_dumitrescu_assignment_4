package Start;
import dataLayer.RestaurantSerializator;
import presentationLayer.chefPanel.ChefGraphicalUI;
import presentationLayer.mainPanel.MainMenuGraphicalUI;
import java.io.IOException;

public class MainClass {
    public static String arg ="";

    public static void main(String args[]) throws IOException, ClassNotFoundException {
        new MainMenuGraphicalUI();
        arg = args[0];
        new ChefGraphicalUI();
        RestaurantSerializator.readFile();
    }
}
