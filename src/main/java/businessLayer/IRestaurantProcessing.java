package businessLayer;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.Collection;
import java.util.List;

public interface IRestaurantProcessing {

    // administrator

    public void createNewItem(MenuItem item) throws IOException, ClassNotFoundException;
    public void deleteItem(int index);
    public void editMenuItem(int newPrice,String newName,String description,int index);

    // waiter

    public void createNewOrder(Order order, List<MenuItem> items); // probable mistake(list)
    public int computePriceForOrder(Order order);
    public void generateBill(List<MenuItem> itemsOrdered) throws FileNotFoundException;
}
