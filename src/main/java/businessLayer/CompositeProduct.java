package businessLayer;

import java.io.Serializable;

public class CompositeProduct{
    public String name;
    public String[] menuItems = new String[1000];
    String description;
    int[] baseProductsPrice = new int[1000];

    public CompositeProduct(String name,MenuItem[] product,String description){
        int index = 0 ;
        this.name = name;
        while(index < product.length){
            this.menuItems[index] = product[index].name;
            this.baseProductsPrice[index] = product[index].price;
            index ++;
        }
        this.description = description;
    }

    public int computePrice(){
        int index = 0;
        int productPrice = 0;
        while(index < baseProductsPrice.length){
            productPrice += this.baseProductsPrice[index];
            index ++;
        }
        return productPrice;
    }
}
