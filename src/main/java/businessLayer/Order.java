package businessLayer;

public class Order {
    int OrderID;
    String date;
    int tableID;
    int hash = 0;

    public Order(int OrderID, String date, int tableID){
        this.OrderID = OrderID;
        this.date = date;
        this.tableID = tableID;
    }

    public int hashCode(){
        hash = this.OrderID ;
        return hash;
    }
}
