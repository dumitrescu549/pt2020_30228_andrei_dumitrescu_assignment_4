package businessLayer;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;
import java.util.List;
/**Clasa Restaurant, cu implementarea metodelor din interfata IRestaurantProcessing**/
public class Restaurant implements IRestaurantProcessing, Serializable {
    public static  List<MenuItem> menu = new ArrayList<MenuItem>();
    public Map <Order, List<MenuItem>> orderInfo = new HashMap<>();
    int billID = 0;


    /** Adaugarea unui obiect in meniu
     * @param item
     * Obiectul pe care dorim sa-l adaugam*/
    @Override
    public void createNewItem(MenuItem item) {
        menu.add(item);
    }

    /**Stergerea unui obiect din meniu
     * @param index
     * Indexul la care se afla obiectul pe care dorim sa-l stergem din meniu*/
    @Override
    public void deleteItem(int index) {
        menu.remove(index);
    }

    /**Editarea unui obiect din meniu
     * @param newPrice
     * Noul pret pe care dorim sa-l adaugam
     * @param newName
     * Noul nume pe care dorim sa-l atribuim obiectului
     * @param description
     * Descrierea obiectului
     * @param index
     * Index-ul la care se afla obiectul pe care dorim sa-l editam*/
    @Override
    public void editMenuItem(int newPrice,String newName,String description,int index) {
        menu.remove(index);
        MenuItem newItem = new MenuItem(newName,newPrice,description);
        menu.add(index,newItem);
    }

    /**Creearea de noi comenzi
     * @param order
     * Comanda pe care dorim sa o adaugam in lista de comenzi
     * @param items
     * Lista de obiecte care a fost comandata de catre client*/
    @Override
    public void createNewOrder(Order order,List<MenuItem> items) { // probable mistake (list)
        orderInfo.put(order,items);
    }


    /**Calculul notei comenzii
     * @param order
     * Comanda pentru care trebuie sa calculam totalul de plata*/
    @Override
    public int computePriceForOrder(Order order) {
        int price = 0;
        for(MenuItem item : orderInfo.get(order)){
            price += item.price;
        }
        return price;
    }

    /**Generarea chitantei in format .txt
     * @param itemsOrdered
     * Obiectele care au fost cumparate(si pentru care se vor face nota)
     * @throws FileNotFoundException
     * In cazul in care nu se poate creea chitanta*/
    @Override
    public void generateBill(List<MenuItem> itemsOrdered) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter("Bill for order "+(billID++) +".txt");
        int total = 0;
        for(int i = 0 ; i < itemsOrdered.size();++i){
            pw.println(itemsOrdered.get(i).name+" ---- " +itemsOrdered.get(i).price +"lei");
            total += itemsOrdered.get(i).price;
        }
        pw.println("TOTAL: " + total + " Lei ");
        pw.close();

    }
}
