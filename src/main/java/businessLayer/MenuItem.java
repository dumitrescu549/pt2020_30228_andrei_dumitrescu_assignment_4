package businessLayer;

import java.io.Serializable;

public class MenuItem implements Serializable {

    private static final long serialVersionUID = 7612145358587238580L;
    public String name;
    public int price;
    public String description;

    public MenuItem(String name, int price,String description){
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public int computePrice(){
        return this.price;
    }

}
