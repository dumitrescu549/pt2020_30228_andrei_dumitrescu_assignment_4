package businessLayer;

import java.io.Serializable;

public class BaseProduct {
    public String name;
    public int price;
    public String description;

    public BaseProduct(String name, int price,String description){
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public int computePrice(){
        return this.price;
    }
}
