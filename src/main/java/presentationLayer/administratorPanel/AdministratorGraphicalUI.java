package presentationLayer.administratorPanel;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.FileWriter;
import dataLayer.RestaurantSerializator;
import presentationLayer.administratorPanel.CreateBaseProductButton.IntroduButtonListener;
import presentationLayer.administratorPanel.CreateBaseProductButton.b1ButtonListener;
import presentationLayer.administratorPanel.CreateCompositeProduct.b2ButtonListener;
import presentationLayer.administratorPanel.DeleteItemFromMenuButton.b4ButtonListener;
import presentationLayer.administratorPanel.EditItemFromMenuButton.b3ButtonListener;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.IOException;
import java.io.Serializable;

public class AdministratorGraphicalUI implements Serializable {

    JFrame frame = new JFrame("ADMINISTRATOR");
    JLabel l1 = new JLabel("ADMINISTRATOR");
    JLabel l2 = new JLabel ("MENU");
    JButton b1 = new JButton("Create Base Product");
    JButton b2 = new JButton("Create Composite Product");
    JButton b3 = new JButton("Edit item from menu");
    JButton b4 = new JButton("Delete item from menu");
    public static JTable table;
    public static Restaurant restaurant = new Restaurant();


    public static DefaultTableModel model = new DefaultTableModel();

    public AdministratorGraphicalUI() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(900,600);
        this.l1.setBounds(385,0,150,50);
        this.l2.setBounds(415,150,150,50);
        this.b1.setBounds(15,75,200,50);
        this.b2.setBounds(225,75,200,50);
        this.b3.setBounds(435,75,200,50);
        this.b4.setBounds(645,75,200,50);

        this.b1.addActionListener(new b1ButtonListener());
        this.b2.addActionListener(new b2ButtonListener());
        this.b3.addActionListener(new b3ButtonListener());
        this.b4.addActionListener(new b4ButtonListener());

        table = new JTable();
        String[] columnNames = {"Name","Price","Ingredients"};
        model.setColumnIdentifiers(columnNames);
        table.setModel(model);
        table.setBounds(150,200,600,300);
        JScrollPane jp = new JScrollPane(table);

        for(int i = 0; i < Restaurant.menu.size();++i){
            Object data[] = new Object[3];
            data[0] = Restaurant.menu.get(i).name;
            data[1] = Restaurant.menu.get(i).price;
            data[2] = Restaurant.menu.get(i).description;
            model.addRow(data);
        }

        panel.add(table);
        frame.add(l1);
        frame.add(l2);
        frame.add(b1);
        frame.add(b2);
        frame.add(b3);
        frame.add(b4);
        frame.add(panel);
        frame.show();
    }
}
