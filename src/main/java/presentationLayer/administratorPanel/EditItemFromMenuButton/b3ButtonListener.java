package presentationLayer.administratorPanel.EditItemFromMenuButton;

import dataLayer.FileWriter;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class b3ButtonListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {

        int i = AdministratorGraphicalUI.table.getSelectedRow();
        String newName = (String)AdministratorGraphicalUI.model.getValueAt(i,0);
        int newPrice = Integer.parseInt(AdministratorGraphicalUI.model.getValueAt(i,1).toString());
        String description = AdministratorGraphicalUI.model.getValueAt(i,2).toString();
        AdministratorGraphicalUI.restaurant.editMenuItem(newPrice,newName,description,i);
        try {
            FileWriter.writeToFile(AdministratorGraphicalUI.restaurant.menu);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
