package presentationLayer.administratorPanel.CreateBaseProductButton;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import dataLayer.FileWriter;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;
import presentationLayer.administratorPanel.CreateCompositeProduct.b2ButtonListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;

public class IntroduButtonListener implements ActionListener,Serializable {
    JTextField tf1;
    JTextField tf2;
    MenuItem item;



    public IntroduButtonListener(JTextField tf1, JTextField tf2){
        this.tf1 = tf1;
        this.tf2 = tf2;
    }
    public void actionPerformed(ActionEvent e)

    {
        String name = tf1.getText();
        int price =Integer.parseInt(tf2.getText());
        String description = "-";
        BaseProduct base  = new BaseProduct(name,price,description);
        item = new MenuItem(base.name,base.price,base.description);
        AdministratorGraphicalUI.restaurant.createNewItem(item);

        try {
            FileWriter.writeToFile(AdministratorGraphicalUI.restaurant.menu);
        } catch (IOException ex) {
            System.out.println("a");
        }

        Object[] data = new Object[3];
        data[0] = item.name;
        data[1] = item.computePrice();
        data[2] = "-";


        AdministratorGraphicalUI.model.addRow(data);
    }
}
