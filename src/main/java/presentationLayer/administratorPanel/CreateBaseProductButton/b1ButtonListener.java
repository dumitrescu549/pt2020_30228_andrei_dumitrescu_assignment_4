package presentationLayer.administratorPanel.CreateBaseProductButton;

import businessLayer.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class b1ButtonListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        JFrame frame = new JFrame("Create Base Product");
        JPanel panel = new JPanel();
        JButton introdu = new JButton("Introdu");
        JTextField tf1 = new JTextField(50);
        JLabel l1 = new JLabel("Nume:");
        JLabel l2 = new JLabel("Pret:");
        JTextField tf2 = new JTextField(50);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,300);
        l1.setBounds(10,10,50,25);
        l2.setBounds(10,100,50,25);
        tf1.setBounds(60,10,100,25);
        tf2.setBounds(60,100,100,25);
        introdu.setBounds(270,55,100,25);
        introdu.addActionListener(new IntroduButtonListener(tf1,tf2));
        frame.add(tf1);
        frame.add(tf2);
        frame.add(l1);
        frame.add(l2);
        frame.add(introdu);
        frame.add(panel);
        frame.show();
    }
}
