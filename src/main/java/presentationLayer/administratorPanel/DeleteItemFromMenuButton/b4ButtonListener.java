package presentationLayer.administratorPanel.DeleteItemFromMenuButton;

import dataLayer.FileWriter;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;
import presentationLayer.administratorPanel.CreateCompositeProduct.b2ButtonListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class b4ButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        int i = AdministratorGraphicalUI.table.getSelectedRow();
        AdministratorGraphicalUI.restaurant.deleteItem(i);
        try {
            FileWriter.writeToFile(AdministratorGraphicalUI.restaurant.menu);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        AdministratorGraphicalUI.model.removeRow(i);

    }
}
