package presentationLayer.administratorPanel.CreateCompositeProduct;

import businessLayer.BaseProduct;

import javax.swing.*;
import java.awt.*;
import businessLayer.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class b2ButtonListener implements ActionListener {

    public static String productName;

    public void actionPerformed(ActionEvent e) {
        JFrame frame = new JFrame("Create Composite Product");
        JPanel panel = new JPanel();
        JLabel l1 = new JLabel("Name the item that you want to create in the box down below and choose it's ingredients from the menu by");
        JLabel l2 = new JLabel("clicking on them. Use 'Enter item' after you click on an item, to select it. Press done when you are done ");
        JLabel l4 = new JLabel("selecting items and want to add it to the menu.");
        JLabel l3 = new JLabel("Name:");
        JTextField tf1 = new JTextField(50);
        JButton b1 = new JButton("Enter name");
        JButton b2 = new JButton("Enter item");
        JButton b3 = new JButton("Done");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700,300);

        l1.setBounds(10,20,600,35);
        l2.setBounds(10,45,600,35);
        l3.setBounds(10,110,50,35);
        l4.setBounds(10,70,600,35);
        tf1.setBounds(50,115,300,25);
        b1.setBounds(10,200,150,35);
        b2.setBounds(250,200,150,35);
        b3.setBounds(520,200,150,35);

        b1.addActionListener(new EnterNameButtonListener(tf1));
        b2.addActionListener(new EnterItemButtonListener());
        b3.addActionListener(new DoneButtonListener());



        frame.add(l1);
        frame.add(l2);
        frame.add(l3);
        frame.add(l4);
        frame.add(tf1);
        frame.add(b1);
        frame.add(b2);
        frame.add(b3);
        frame.add(panel);
        frame.show();

    }
}
