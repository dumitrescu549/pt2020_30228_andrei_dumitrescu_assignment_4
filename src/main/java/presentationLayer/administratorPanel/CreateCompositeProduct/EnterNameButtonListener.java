package presentationLayer.administratorPanel.CreateCompositeProduct;

import presentationLayer.administratorPanel.AdministratorGraphicalUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EnterNameButtonListener implements ActionListener {
    JTextField tf;
    public EnterNameButtonListener(JTextField tf){
        this.tf = tf;
    }

    public void actionPerformed(ActionEvent e) {
        b2ButtonListener.productName = tf.getText();
    }
}
