package presentationLayer.administratorPanel.CreateCompositeProduct;

import businessLayer.BaseProduct;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

import businessLayer.MenuItem;
public class EnterItemButtonListener implements ActionListener {
    public static ArrayList<MenuItem> selectedItems = new ArrayList<MenuItem>();

    public void actionPerformed(ActionEvent e) {
        int i = AdministratorGraphicalUI.table.getSelectedRow();
        String name = (String)AdministratorGraphicalUI.model.getValueAt(i,0);
        String pret = AdministratorGraphicalUI.model.getValueAt(i,1).toString();
        String descriere = AdministratorGraphicalUI.model.getValueAt(i,2).toString();
        int price = Integer.parseInt(pret);
        MenuItem bp = new MenuItem(name,price,descriere);
        selectedItems.add(bp);

    }
}
