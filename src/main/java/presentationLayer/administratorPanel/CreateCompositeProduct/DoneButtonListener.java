package presentationLayer.administratorPanel.CreateCompositeProduct;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import dataLayer.FileWriter;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;

public class DoneButtonListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        MenuItem[] prodcts = new MenuItem[EnterItemButtonListener.selectedItems.size()];
        String description = "";
        for(int i = 0; i < EnterItemButtonListener.selectedItems.size() ;i++){
            prodcts[i] = EnterItemButtonListener.selectedItems.get(i);
            if(i == EnterItemButtonListener.selectedItems.size()-1){
                description += EnterItemButtonListener.selectedItems.get(i).name;
            }
            else{
                description += EnterItemButtonListener.selectedItems.get(i).name +", ";
            }

        }


        CompositeProduct product = new CompositeProduct(b2ButtonListener.productName,prodcts,description);
        Object data[] = new Object[3];
        int index = 0;
        data[0] = product.name;
        data[1] = product.computePrice();
        data[2] = description;
        AdministratorGraphicalUI.restaurant.createNewItem(new MenuItem(product.name,product.computePrice(),description));// adaugarea compozitului in meniu
        try {
            FileWriter.writeToFile(AdministratorGraphicalUI.restaurant.menu);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        EnterItemButtonListener.selectedItems.clear();
        AdministratorGraphicalUI.model.addRow(data);
    }
}
