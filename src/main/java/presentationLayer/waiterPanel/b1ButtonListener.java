package presentationLayer.waiterPanel;

import java.awt.*;
import java.awt.event.ActionEvent;
import businessLayer.MenuItem;
import java.awt.event.ActionListener;

public class b1ButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        int i = WaiterGraphicalUI.tableMenu.getSelectedRow();
        String name = WaiterGraphicalUI.model1.getValueAt(i,0).toString();
        int price = Integer.parseInt(WaiterGraphicalUI.model1.getValueAt(i,1).toString());
        String description = WaiterGraphicalUI.model1.getValueAt(i,2).toString();

        MenuItem itemOrdered = new MenuItem(name,price,description);

        WaiterGraphicalUI.itemsOrdered.add(itemOrdered);
    }
}
