package presentationLayer.waiterPanel;

import businessLayer.Order;
import presentationLayer.chefPanel.ChefGraphicalUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Date;

public class b2ButtonListener implements ActionListener {
    int orderID = 0;
    @Override
    public void actionPerformed(ActionEvent e) {
        Date date = new Date();
        Order newOrder = new Order(orderID++,date.toString(),1);
        WaiterGraphicalUI.restaurant.createNewOrder(newOrder,WaiterGraphicalUI.itemsOrdered);

        Object data[] = new Object[3];
        data[0] = newOrder.hashCode();
        data[1] = date.toString();
        data[2] = WaiterGraphicalUI.restaurant.computePriceForOrder(newOrder);

        WaiterGraphicalUI.model2.addRow(data);
        try {
            WaiterGraphicalUI.restaurant.generateBill(WaiterGraphicalUI.itemsOrdered);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        ChefGraphicalUI.update(WaiterGraphicalUI.itemsOrdered);
        WaiterGraphicalUI.itemsOrdered.clear();


    }
}
