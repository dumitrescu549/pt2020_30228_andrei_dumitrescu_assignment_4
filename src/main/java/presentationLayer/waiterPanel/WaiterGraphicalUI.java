package presentationLayer.waiterPanel;

import businessLayer.MenuItem;
import businessLayer.Restaurant;
import presentationLayer.administratorPanel.AdministratorGraphicalUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import businessLayer.MenuItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WaiterGraphicalUI{

    JFrame frame = new JFrame("WAITER");
    JLabel l1 = new JLabel("WAITER");
    JLabel l2 = new JLabel("MENU");
    JLabel l3 = new JLabel("ORDERS");
    JButton b1 = new JButton("Add To Order");
    JButton b2 = new JButton("Done Ordering");
    int index = 0;
    public static JTable tableOrder;
    public static JTable tableMenu;
    public static List<MenuItem> itemsOrdered = new ArrayList<MenuItem>();
    public static Restaurant restaurant = new Restaurant();


    public static DefaultTableModel model1 = new DefaultTableModel();
    public static DefaultTableModel model2 = new DefaultTableModel();

    public WaiterGraphicalUI(){
        JPanel panel = new JPanel();
        panel.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800,600);
        this.l1.setBounds(360,0,150,50);
        this.l2.setBounds(175,150,150,35);
        this.l3.setBounds(600,150,150,35);
        this.b1.setBounds(200,75,150,35);
        this.b2.setBounds(400,75,150,35);

        this.b1.addActionListener(new b1ButtonListener());
        this.b2.addActionListener(new b2ButtonListener());

        tableMenu = new JTable();
        String[] columnNames = {"Name","Price","Description"};
        model1.setColumnIdentifiers(columnNames);
        tableMenu.setModel(model1);
        tableMenu.setBounds(15,200,375,350);
        JScrollPane jp = new JScrollPane(tableMenu);

        while(index < AdministratorGraphicalUI.restaurant.menu.size()){
            Object[] data = new Object[3];
            data[0] = AdministratorGraphicalUI.restaurant.menu.get(index).name;
            data[1] = AdministratorGraphicalUI.restaurant.menu.get(index).price;
            data[2] = AdministratorGraphicalUI.restaurant.menu.get(index).description;
            model1.addRow(data);
            index++;
        }

        tableOrder = new JTable();
        String[] columnNamesOrder = {"Order Hascode","Date","Total"};
        model2.setColumnIdentifiers(columnNamesOrder);
        tableOrder.setModel(model2);
        tableOrder.setBounds(400,200,375,350);
        JScrollPane jpOrder = new JScrollPane(tableOrder);


        panel.add(tableOrder);
        panel.add(tableMenu);
        frame.add(l1);
        frame.add(l2);
        frame.add(l3);
        frame.add(b1);
        frame.add(b2);
        frame.add(panel);
        frame.show();
    }
}
