package presentationLayer.chefPanel;

import businessLayer.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.List;

public class ChefGraphicalUI {

    JFrame frame = new JFrame("CHEF");
    JLabel l1 = new JLabel("CHEF");
    public static JTextField tf1 = new JTextField(50);
    JLabel l2 = new JLabel("Actual Order");
    public static DefaultTableModel model = new DefaultTableModel();
    public static JTable table;
    public static int orderID =0;

    public ChefGraphicalUI(){
        JPanel panel = new JPanel();
        panel.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,600);
        this.l1.setBounds(260,0,150,50);
        this.tf1.setEditable(false);
        this.tf1.setText("No order available.");
        this.tf1.setBounds(10,100,200,25);
        this.l2.setBounds(240,150,150,50);

        table = new JTable();
        String[] columnNames = {"Order"};
        model.setColumnIdentifiers(columnNames);
        table.setModel(model);
        table.setBounds(100,200,375,300);
        JScrollPane jp = new JScrollPane(table);

        panel.add(table);
        frame.add(l1);
        frame.add(l2);
        frame.add(tf1);
        frame.add(panel);
        frame.show();
    }

    public static void update(List<MenuItem> itemsOrdered){
        ChefGraphicalUI.model.setRowCount(0);
        ChefGraphicalUI.tf1.setText("NEW ORDER WITH ID " + (ChefGraphicalUI.orderID++) +" !");
        for(int i = 0 ; i < itemsOrdered.size();++i){
            Object data[] = new Object[1];
            data[0] = itemsOrdered.get(i).name;
            ChefGraphicalUI.model.addRow(data);
        }

    }

}
