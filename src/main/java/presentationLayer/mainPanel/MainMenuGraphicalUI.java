package presentationLayer.mainPanel;

import javax.swing.*;

public class MainMenuGraphicalUI {
    JFrame frame = new JFrame("Main Menu");
    JButton b1 = new JButton("ADMINISTRATOR");
    JButton b2 = new JButton("WAITER");
    JLabel l1 = new JLabel("MAIN MENU");

    public MainMenuGraphicalUI(){
        JPanel panel = new JPanel();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,400);
        this.l1.setBounds(265,0,400,100);
        this.b1.setBounds(100,100,400,50);
        this.b2.setBounds(100,200,400,50);
        this.b1.addActionListener(new b1ButtonListener());
        this.b2.addActionListener(new b2ButtonListener());
        frame.add(b1);
        frame.add(b2);
        frame.add(l1);
        frame.add(panel);
        frame.show();
    }
}
